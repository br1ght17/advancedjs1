class Employee{
    constructor(name,age,salary){
        this._name = name
        this._age = age
        this._salary = salary
    }
    set name(newName){
        newName = newName.trim()
        if(!newName){
            throw "Name can't be empty"
        }
        this._name = newName
    }
    get name(){
        return this._name
    }
    set salary(newSalary){
        if(newSalary>2500 || typeof newSalary != "number"){
            throw "You are not good enough to have this salary"
        }
        this._salary = newSalary
    }
    get salary(){
        return this._salary + " $/month"
    }
    set age(ageCheck){
        if(18>=ageCheck || ageCheck>60 || typeof ageCheck != "number"){
            throw 'You are not in ours company preferences'
        }
        this._age = ageCheck
    }
    get age(){
        return this._age
    }
}

class Programmer extends Employee{
    constructor(name,age,salary,lang){
        super(name,age,salary)
        this.lang = lang
    }
    get salary(){
        return this._salary * 3
    }
    set salary(newSalary){
        if(newSalary>2500 || typeof newSalary != "number"){
            throw "You are not good enough to have this salary"
        }
        this._salary = newSalary
    }
}

let dawg = new Programmer('Dude' , 27 , 1500 , ['js' , 'go' , 'view'])
console.log(dawg)